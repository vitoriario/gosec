package main

import (
	"io/ioutil"
	"os"
	"reflect"
	"strings"
	"testing"
)

func TestCleanPath(t *testing.T) {
	tests := []struct {
		name string
		path string
		want string
	}{
		{
			name: "undecorated path",
			path: "gosec-config.json",
			want: "gosec-config.json",
		},
		{
			name: "dot decorated path",
			path: "./gosec-config.json",
			want: "gosec-config.json",
		},
		{
			name: "double-dot decorated path",
			path: "../gosec-config.json",
			want: "gosec-config.json",
		},
		{
			name: "double-dot decorated nested path",
			path: "./foo/bar/../../gosec-config.json",
			want: "gosec-config.json",
		},
		{
			name: "absolute path",
			path: "/tmp/config.json",
			want: "/tmp/config.json",
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got := cleanPath(tt.path)
			if !reflect.DeepEqual(tt.want, got) {
				t.Fatalf("Expected %v, but got: %v", tt.want, got)
			}
		})
	}
}

func TestCheckConfig(t *testing.T) {
	tests := []struct {
		name    string
		path    string
		wantErr bool
	}{
		{
			name:    "existing file",
			path:    "CHANGELOG.md",
			wantErr: false,
		},
		{
			name:    "existing nested file",
			path:    "plugin/plugin.go",
			wantErr: false,
		},
		{
			name:    "missing file",
			path:    "./boop",
			wantErr: true,
		},
		{
			name:    "existing directory",
			path:    "test",
			wantErr: true,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			err := checkConfig(tt.path)
			if (err != nil) != tt.wantErr {
				t.Fatalf("checkConfig() for path: %v, unexpected error: %v", tt.path, err)
			}
		})
	}
}

func TestPassthroughFile(t *testing.T) {
	tests := []struct {
		name     string
		path     string
		filename string
		want     []string
	}{
		{
			name:     "existing file",
			path:     "./",
			filename: "README.md",
			want:     []string{"-conf", "README.md"},
		},
		{
			name:     "nested file",
			path:     "test/fixtures",
			filename: "gosec_output.json",
			want:     []string{"-conf", "test/fixtures/gosec_output.json"},
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := passthroughFile(tt.path, tt.filename)
			if !reflect.DeepEqual(tt.want, got) {
				if err != nil {
					t.Fatalf("passthroughFile() for filename: %v, unexpected error: %v", tt.filename, err)
				}
				t.Fatalf("Expected %v, but got: %v", tt.want, got)
			}
		})
	}
}

func TestPassthroughRaw(t *testing.T) {
	raw := "filecontents"

	got, err := passthroughRaw(raw)
	if err != nil {
		t.Fatalf("passthroughRaw() for raw: %v, unexpected error: %v", raw, err)
	}
	defer os.Remove(got[1])

	if len(got) != 2 {
		t.Fatalf("Expected passthroughRaw() len to be 2, but got: %v", len(got))
	}
	if got[0] != "-conf" {
		t.Fatalf("Expected '-conf', but got: %v", got)
	}
	if !strings.Contains(got[1], "gosec") {
		t.Fatalf("Expected filename to match gosec, but got: %v", got[1])
	}
	if _, err := os.Stat(got[1]); err != nil {
		t.Fatalf("Expected tempfile to exist, but got: %v", err)
	}
	dat, err := ioutil.ReadFile(got[1])
	if err != nil {
		t.Fatalf("passthroughRaw() tempfile cannot be read, unexpected error: %v", err)
	}
	if string(dat) != raw {
		t.Fatalf("passthroughRaw() tempfile does not match raw: %v", raw)
	}
}
